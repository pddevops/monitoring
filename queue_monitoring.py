import requests
import json
import sys
import urllib
from requests.auth import HTTPBasicAuth
import requests
import traceback
from smtplib import SMTP_SSL as SMTP
from smtplib import SMTP_SSL as SMTP
from email.MIMEText import MIMEText
import smtplib
import datetime
import os
#from __future__ import print_function

#----------------------------------------------------------------------HIPCHAT--------------------------------------------------------------------------#
hipchat_token = "At3aH5XSoEqDNg255TmwjWkJSLVuhczIlerfh4NE"
room_id = '3417928'

def hipchat_notify(token, room, message, color='yellow', notify=False,
				   format='text', host='api.hipchat.com'):

	if len(message) > 10000:
		raise ValueError('Message too long')
	if format not in ['text', 'html']:
		raise ValueError("Invalid message format '{0}'".format(format))
	if color not in ['yellow', 'green', 'red', 'purple', 'gray', 'random']:
		raise ValueError("Invalid color {0}".format(color))
	if not isinstance(notify, bool):
		raise TypeError("Notify must be boolean")

	url = "https://{0}/v2/room/{1}/notification".format(host, room)
	headers = {'Content-type': 'application/json'}
	headers['Authorization'] = "Bearer " + token
	payload = {
		'message': message,
		'notify': notify,
		'message_format': format,
		'color': color
	}
	r = requests.post(url, data=json.dumps(payload), headers=headers)
	r.raise_for_status()


#-------------------------------------------------------Config.json file containing the email sent status------------------------------------------------#
#path = os.getcwd()
#path = path +"/config.json"
path = "/home/ankit/monitoring/config.json"
with open(path,"r") as data_file:
	data_json = json.load(data_file)
	print data_json

#-----------------------------------------------------------------API----------------------------------------------------------------------------------#
url = 'http://mq.paralleldots.com:15672/api/queues'

resp = requests.get(url, auth=HTTPBasicAuth('ankit', 'root53')).json()

#--------------------------------------------------------------------Threshold Values------------------------------------------------------------------#

annotater_threshold = 2000
bloomfilter_threshold = 200000
crawler_threshold = 1000
nasscom_threshold = 500

twitter_threshold = 1000
tweets_queue_threshold = 1000 

#---------------------------------------------------------------------Mailing list--------------------------------------------------------------------#

news_mailing_list = ["ankit@paralleldots.com","kushank@paralleldots.com","ahwan@paralleldots.com","meghdeepr@paralleldots.com"]
twitter_mailing_list = ["ankit@paralleldots.com","kushank@paralleldots.com","ahwan@paralleldots.com","richa@paralleldots.com","deepak@paralleldots.com"]
# news_mailing_list = ["nirjhari@paralleldots.com"]
# twitter_mailing_list = ["nirjhari@paralleldots.com"]

#-----------------------------------------------------------------------------Sendmail------------------------------------------------------------------#
def sendmailText(toaddrs,subject,content):
	fromaddr = 'monitoring@paralleldots.com'
	msg      = MIMEText(content, 'plain')
	msg['Subject']= subject
	#Change according to your settings
	smtp_server = 'email-smtp.us-east-1.amazonaws.com'
	smtp_username = 'AKIAJGOCUI3XCM6PB5GQ'
	smtp_password = 'AkvGPOUteruQci14heSn1/ZjpKGVz/vS7BdzCdpgIHwR'
	smtp_port = '2587'
	smtp_do_tls = True

	server = smtplib.SMTP(
		host = smtp_server,
		port = smtp_port,
		timeout = 10
	)
	server.set_debuglevel(10)
	server.starttls()
	server.ehlo()
	server.login(smtp_username, smtp_password)
	server.sendmail(fromaddr, toaddrs, msg.as_string())
	print server.quit()

#------------------------------------------------------------------News Monitoring---------------------------------------------------------------------#

for data in resp:
	name = data["name"]
	if name == "annotater":
		messages_count = data["messages"]
		print messages_count
		if messages_count > annotater_threshold:
			if data_json["annotater"] == "":
				sendmailText(news_mailing_list,"Urgent: Annotater Queue Overflow","Annotater queue has more than "+str(annotater_threshold)+" news queued up. Please check")
				now = datetime.datetime.now()
				data_json["annotater"] = str(now)
				with open(path, 'w') as out_file:
					json.dump(data_json, out_file)

				try:
					hipchat_notify(hipchat_token, room_id, 'Annotater Queue Overflow')
				except Exception as e:
					msg = "[ERROR] HipChat notify failed: '{0}'".format(e)
					print msg
					sys.exit(1)
			else:
				mail_time = data_json["annotater"]
				#print time_of_mail
				now = datetime.datetime.now()
				print now
				current = datetime.datetime.strptime(mail_time,"%Y-%m-%d %H:%M:%S.%f")
				print current
				diff = now - current
				if diff.seconds > 3600:
					sendmailText(news_mailing_list,"Urgent: Annotater Queue Overflow","Annotater queue has more than "+str(annotater_threshold)+" news queued up. Please check")
					now = datetime.datetime.now()
					data_json["annotater"] = str(now)
					with open(path, 'w') as out_file:
						json.dump(data_json, out_file)
					try:
						hipchat_notify(hipchat_token, room_id, 'Annotater Queue Overflow')
					except Exception as e:
						msg = "[ERROR] HipChat notify failed: '{0}'".format(e)
						print msg
						sys.exit(1)
		else:
			print "Queue is filled with news lesser than threshold value of "+str(annotater_threshold)+"."
	elif name == "bloomfilter2.0":
		messages_count = data["messages"]
		print messages_count
		if messages_count > bloomfilter_threshold:
			if data_json["bloomfilter"] == "":
				sendmailText(news_mailing_list,"Urgent: Bloomfilter Queue Overflow","Bloomfilter queue has more than "+str(bloomfilter_threshold)+" news queued up. Please check")
				now = datetime.datetime.now()
				data_json["bloomfilter"] = str(now)
				with open(path, 'w') as out_file:
					json.dump(data_json, out_file)
				try:
					hipchat_notify(hipchat_token, room_id, 'Bloomfilter Queue Overflow')
				except Exception as e:
					msg = "[ERROR] HipChat notify failed: '{0}'".format(e)
					print msg
					sys.exit(1)
			else:
				mail_time = data_json["bloomfilter"]
				#print time_of_mail
				now = datetime.datetime.now()
				print now
				current = datetime.datetime.strptime(mail_time,"%Y-%m-%d %H:%M:%S.%f")
				print current
				diff = now - current
				if diff.seconds > 3600:
					sendmailText(news_mailing_list,"Urgent: Bloomfilter Queue Overflow","Bloomfilter queue has more than "+str(bloomfilter_threshold)+" news queued up. Please check")
					now = datetime.datetime.now()
					data_json["bloomfilter"] = str(now)
					
					with open(path, 'w') as out_file:
						json.dump(data_json, out_file)

					try:
						hipchat_notify(hipchat_token, room_id, 'Bloomfilter Queue Overflow')
					except Exception as e:
						msg = "[ERROR] HipChat notify failed: '{0}'".format(e)
						print msg 
						sys.exit(1)	
		else:
			print "Queue is filled with news lesser than threshold value of "+str(bloomfilter_threshold)
	elif name == "crawler2.0":
		messages_count = data["messages"]
		print messages_count
		if messages_count > crawler_threshold:
			if data_json["crawler"] == "":
				sendmailText(news_mailing_list,"Urgent: Crawler Queue Overflow","Crawler queue has more than "+str(crawler_threshold)+" news queued up. Please check")
				now = datetime.datetime.now()
				data_json["crawler"] = str(now)
				with open(path, 'w') as out_file:
					json.dump(data_json, out_file)
				try:
					hipchat_notify(hipchat_token, room_id, 'Crawler Queue Overflow')
				except Exception as e:
					msg = "[ERROR] HipChat notify failed: '{0}'".format(e)
					print msg
					sys.exit(1)
			else:
				mail_time = data_json["crawler"]
				#print time_of_mail
				now = datetime.datetime.now()
				print now
				current = datetime.datetime.strptime(mail_time,"%Y-%m-%d %H:%M:%S.%f")
				print current
				diff = now - current
				if diff.seconds > 3600:
					sendmailText(news_mailing_list,"Urgent: Crawler Queue Overflow","Crawler queue has more than"+str(crawler_threshold)+"news queued up. Please check")
					now = datetime.datetime.now()
					data_json["crawler"] = str(now)
					with open(path, 'w') as out_file:
						json.dump(data_json, out_file)
					try:
						hipchat_notify(hipchat_token, room_id, 'Crawler Queue Overflow')
					except Exception as e:
						msg = "[ERROR] HipChat notify failed: '{0}'".format(e)
						print msg
						sys.exit(1)
		else:
			print "Queue is filled with news lesser than threshold value of "+str(crawler_threshold)
	elif name == "nasscom":
		messages_count = data["messages"]
		print messages_count
		if messages_count > nasscom_threshold:
			if data_json["nasscom"] == "":
				sendmailText(news_mailing_list,"Urgent: Nasscom Queue Overflow","Nasscom queue has more than "+str(nasscom_threshold)+" news queued up. Please check")
				now = datetime.datetime.now()
				data_json["nasscom"] = str(now)
				with open(path, 'w') as out_file:
					json.dump(data_json, out_file)
				try:
					hipchat_notify(hipchat_token, room_id, 'Nasscom Queue Overflow')
				except Exception as e:
					msg = "[ERROR] HipChat notify failed: '{0}'".format(e)
					print msg
					sys.exit(1)
			else:
				mail_time = data_json["nasscom"]
				#print time_of_mail
				now = datetime.datetime.now()
				print now
				current = datetime.datetime.strptime(mail_time,"%Y-%m-%d %H:%M:%S.%f")
				print current
				diff = now - current
				if diff.seconds > 3600:
					sendmailText(news_mailing_list,"Urgent: Nasscom Queue Overflow","Nasscom queue has more than "+str(nasscom_threshold)+" news queued up. Please check")
					now = datetime.datetime.now()
					data_json["nasscom"] = str(now)
					with open(path, 'w') as out_file:
						json.dump(data_json, out_file)
					try:
						hipchat_notify(hipchat_token, room_id, 'Nasscom Queue Overflow')
					except Exception as e:
						msg = "[ERROR] HipChat notify failed: '{0}'".format(e)
						print msg
						sys.exit(1)
		else:
			print "Queue is filled with news lesser than threshold value of "+str(nasscom_threshold)

#------------------------------------------------------------------Twitter Monitoring------------------------------------------------------------------#

for data in resp:
	name = data["name"] 
	if name == "twitter":
		messages_count = data["messages"]
		print messages_count
		if messages_count > twitter_threshold:
			if data_json["twitter"] == "":
				sendmailText(twitter_mailing_list,"Urgent: Twitter Queue Overflow","Twitter queue has more than "+str(twitter_threshold)+" tweets queued up. Please check")
				now = datetime.datetime.now()
				data_json["twitter"] = str(now)
				with open(path, 'w') as out_file:
					json.dump(data_json, out_file)
				try:
					hipchat_notify(hipchat_token, room_id, 'Twitter Queue Overflow')
				except Exception as e:
					msg = "[ERROR] HipChat notify failed: '{0}'".format(e)
					print msg
					sys.exit(1)
			else:
				mail_time = data_json["twitter"]
				#print time_of_mail
				now = datetime.datetime.now()
				print now
				current = datetime.datetime.strptime(mail_time,"%Y-%m-%d %H:%M:%S.%f")
				print current
				diff = now - current
				if diff.seconds > 3600:
					sendmailText(twitter_mailing_list,"Urgent: Twitter Queue Overflow","Twitter queue has more than "+str(twitter_threshold)+" tweets queued up. Please check")
					now = datetime.datetime.now()
					data_json["twitter"] = str(now)
					with open(path, 'w') as out_file:
						json.dump(data_json, out_file)	
					try:
						hipchat_notify(hipchat_token, room_id, 'Twitter Queue Overflow')
					except Exception as e:
						msg = "[ERROR] HipChat notify failed: '{0}'".format(e)
						print msg
						sys.exit(1)
		else:
			print "Queue is filled with tweets lesser than threshold value of "+str(twitter_threshold)
	elif name == "tweetsqueeue":
		messages_count = data["messages"]
		print messages_count
		if messages_count > tweets_queue_threshold:
			if data_json["tweetsqueue"] == "":
				sendmailText(twitter_mailing_list,"Urgent: Tweetsqueue Queue Overflow","TweetsQueue has more than "+str(tweets_queue_threshold)+" tweets queued up. Please check")
				now = datetime.datetime.now()
				data_json["tweetsqueue"] = str(now)
				with open(path, 'w') as out_file:
					json.dump(data_json, out_file)

				try:
					hipchat_notify(hipchat_token, room_id, 'Tweetsqueue Queue Overflow')
				except Exception as e:
					msg = "[ERROR] HipChat notify failed: '{0}'".format(e)
					print msg
					sys.exit(1)
			else:
				mail_time = data_json["tweetsqueue"]
				#print time_of_mail
				now = datetime.datetime.now()
				print now
				current = datetime.datetime.strptime(mail_time,"%Y-%m-%d %H:%M:%S.%f")
				print current
				diff = now - current
				if diff.seconds > 3600:
					sendmailText(twitter_mailing_list,"Urgent: Tweetsqueue Queue Overflow","TweetsQueue has more than "+str(tweets_queue_threshold)+" tweets queued up. Please check")
					now = datetime.datetime.now()
					data_json["tweetsqueue"] = str(now)
					with open(path, 'w') as out_file:
						json.dump(data_json, out_file)
					try:
						hipchat_notify(hipchat_token, room_id, 'Tweetsqueue Queue Overflow')
					except Exception as e:
						msg = "[ERROR] HipChat notify failed: '{0}'".format(e)
						print msg 
						sys.exit(1)	
		else:
			print "Queue is filled with tweets lesser than threshold value of "+str(tweets_queue_threshold)
